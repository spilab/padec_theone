import yaml
import os
import re
import glob

def get_rules_by_num(rule_dir: str) -> "dict[int, str]":
    all_rules = glob.glob(os.path.join(rule_dir, "*.yaml"))
    return {
        int(re.findall("Rule([0-9]+).yaml", os.path.basename(rule))[0]): rule for rule in all_rules}

if __name__ == '__main__':
    rule_dir = os.path.join("..", "rules")
    rules_by_prov = {
        1: [1, 3, 5, 7, 9, 11, 13],
        2: [1, 3, 7, 9, 11, 15, 17],
        3: [1, 3, 7, 19, 21],
        4: [3],
        5: [1, 9, 23, 25],
        6: [1, 9, 19, 27],
        7: [1, 5, 11],
        8: [1, 3, 7, 11],
        9: [9, 11, 31, 33],
        10: [1, 3, 7, 35, 37],
        11: [1, 3, 9, 39, 41, 43],
        12: [1, 3, 7, 9, 45, 47]
    }
    rules_by_num = get_rules_by_num(rule_dir)
    for provider in rules_by_prov:
        als = {}
        for rule_num in rules_by_prov[provider]:
            rule = rules_by_num[rule_num]
            with open(rule, 'r') as in_rule:
                loaded_rule = yaml.load(in_rule)
            lock = loaded_rule["lock"]
            endpoint = lock["endpoint"]
            als[endpoint] = als.get(endpoint, []) + lock["accesslevels"]
        locks = [{"lock": {"endpoint": ep, "accesslevels": als[ep]}} for ep in als]
        with open(f'Provider{provider}.yaml', 'w') as out_rule:
            yaml.dump({"locks": locks}, out_rule)

