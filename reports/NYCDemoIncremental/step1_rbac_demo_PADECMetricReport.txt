PADEC metric statistics for scenario step1_rbac_demo
sim_time: 86000.0000
Average request size: 121.0000
Average keyhole size: 492.0000
Average key size: 762.0000
Average data size: 3749.6909
Average percentage of full history sent: 100.0000
Average category of data released as key: 3.0000
Minimum category of data released as key: 3.0000
Average number of attributes released as key: 1.0000
Average precision of data obtained: -Infinity
