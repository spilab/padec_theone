PADEC metric statistics for scenario step4_journal_demo
sim_time: 86000.0000
Average request size: 121.0000
Average keyhole size: 620.0000
Average key size: 891.9010
Average data size: 5880.6566
Average percentage of full history sent: 100.0000
Average category of data released as key: 2.5149
Minimum category of data released as key: 2.0000
Average number of attributes released as key: 1.5149
Average precision of data obtained: -Infinity
