package padec.attribute;

import java.util.List;

public class Interests extends Attribute<List> {

    public static final Integer INTEREST_SPORTS = 1;
    public static final Integer INTEREST_RESTAURANTS = 2;
    public static final Integer INTEREST_MONUMENTS = 3;
    public static final Integer INTEREST_PARKS = 4;

    Interests() {
        super(List.class);
    }
}
