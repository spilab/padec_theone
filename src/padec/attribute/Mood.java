package padec.attribute;

public class Mood extends Attribute<Integer> {

    public static final Integer NEUTRAL = 0;
    public static final Integer HAPPY = 1;
    public static final Integer SAD = 2;
    public static final Integer ANGRY = 3;
    public static final Integer DISGUSTED = 4;
    public static final Integer SURPRISED = 5;
    public static final Integer FEARFUL = 6;

    Mood() {
        super(Integer.class);
    }
}
