package padec.attribute;

public class SoundLevel extends Attribute<Double> {

    SoundLevel() {
        super(Double.class);
    }

}
