package padec.key;

/**
 * List of agreed-upon purposes.
 * I'm not sure if there's a better way of expressing these...
 */
public abstract class Purposes {
    public static final String MARKETING_PURPOSE = "Marketing";
    public static final String TRACKING_PURPOSE = "Tracking";
    public static final String TOURISM_PURPOSE = "Tourism";
}